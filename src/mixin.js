/**
 * @example
 * class FooClass extends mixin(TraitClass, OtherTraitClass) {
 *     // ...
 * }
 * @param MixTrait
 * @param traits
 */
export default function mixin(MixTrait, ...traits) {
    class Mix extends MixTrait {
        constructor(...args) {
            super(...args);

            // copy properties and symbols
            traits.forEach((Trait) => {
                const instance = new Trait(...args);
                copyProps(this, instance);
            });
        }
    }

    // copy static properties and symbols
    traits.forEach((Trait) => {
        copyProps(Mix.prototype, Trait.prototype);
        copyProps(Mix, Trait);
    });

    return Mix;
}

/**
 * Copy properties from source to target.
 * @param target
 * @param source
 */
function copyProps(target, source) {
    const properties = Object.getOwnPropertyNames(source);

    properties
        .concat(Object.getOwnPropertySymbols(source))
        .filter((key) => !key.match(/^(?:constructor|prototype|arguments|caller|name|bind|call|apply|toString|length)$/))
        .forEach((key) => {
            const props = Object.getOwnPropertyDescriptor(source, key);
            Object.defineProperty(target, key, props);
        });
}
