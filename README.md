# Mixin 

[![codebeat badge](https://codebeat.co/badges/791d948f-2fee-42a9-b0d8-bce4beeb672d)](https://codebeat.co/projects/gitlab-com-cherrypulp-libraries-js-mixin-master)
[![Issues](https://img.shields.io/badge/issues-0-brightgreen)](https://gitlab.com/cherrypulp/libraries/js-mixin/issues)
[![License](https://img.shields.io/npm/l/@cherrypulp/mixin)](https://gitlab.com/cherrypulp/libraries/js-mixin/blob/master/LICENSE)
[![npm version](https://badge.fury.io/js/%40cherrypulp%2Fmixin.svg)](https://badge.fury.io/js/%40cherrypulp%2Fmixin)

In JavaScript we can only inherit from a single object. There can be only one `[[Prototype]]` for an object. And a class may extend only one other class.
There’s a concept that can help here, called “mixins”.


## Installation

`npm install @cherrypulp/mixin`


## Quick start

### Define a mixin

```javascript
class FooTrait {
    static bar = 'bar';
    
    constructor() {
        this.baz = this.constructor.bar;
    }
    
    foo() {
        return this.baz;
    }
}
```

### Use a mixin

```javascript
class FooClass extends mixin(FooTrait) {}

const foo = new FooClass();
foo.foo(); // return "bar"
```

Be careful with names collision!

```javascript
class FooClass extends mixin(FooTrait) {
    static bar = 'BAT';
    
    // or
    constructor() {
        super();
        this.baz = 'BAT';
    }
    
    // or
    foo() {
        return 'BAT';
    }
}

const foo = new FooClass();
foo.foo(); // return "BAT"
``` 


## Versioning

Versioned using [SemVer](http://semver.org/).

## Contribution

Please raise an issue if you find any. Pull requests are welcome!

## Author

  + **Stéphan Zych** - [monkeymonk](https://gitlab.com/monkey_monk)

## License

This project is licensed under the MIT License - see the [LICENSE](https://gitlab.com/cherrypulp/libraries/js-mixin/blob/master/LICENSE) file for details.
