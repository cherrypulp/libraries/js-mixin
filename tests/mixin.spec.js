import mixin from '../src/mixin';
import assert from 'assert';
import chai from 'chai';

const expect = chai.expect;
const should = chai.should;

describe('mixin.js', function() {
    describe('class traits', () => {
        it('return a class instance with correct class name', () => {
            class FooClass {}
            class TestClass extends mixin(FooClass) {}

            const instance = new TestClass();

            expect(instance.constructor.name).to.equal('TestClass');
        });

        it('return a class with FooClass methods', () => {
            class FooClass {
                foo() {
                    return 'foo';
                }
            }
            class TestClass extends mixin(FooClass) {}

            const instance = new TestClass();

            expect(instance).to.have.property('foo');
            expect(instance.foo()).to.equal('foo');
        });

        it('return a class with FooClass and BarClass methods', () => {
            class FooClass {
                foo() {
                    return 'foo';
                }
            }
            class BarClass {
                bar() {
                    return 'bar';
                }
            }
            class TestClass extends mixin(FooClass, BarClass) {}

            const instance = new TestClass();

            expect(instance).to.have.property('foo');
            expect(instance.foo()).to.equal('foo');
            expect(instance).to.have.property('bar');
            expect(instance.bar()).to.equal('bar');
        });

        it('return a class with FooClass properties', () => {
            class FooClass {
                constructor() {
                    this.foo = 'foo';
                }
            }
            class TestClass extends mixin(FooClass) {}

            const instance = new TestClass();

            expect(instance).to.have.property('foo');
            expect(instance.foo).to.equal('foo');
        });

        it('return a class with FooClass and BarClass properties', () => {
            class FooClass {
                constructor() {
                    this.foo = 'foo';
                }
            }
            class BarClass {
                constructor() {
                    this.bar = 'bar';
                }
            }
            class TestClass extends mixin(FooClass, BarClass) {}

            const instance = new TestClass();

            expect(instance).to.have.property('foo');
            expect(instance.foo).to.equal('foo');
            expect(instance).to.have.property('bar');
            expect(instance.bar).to.equal('bar');
        });

        it('return a class with FooClass properties overridden', () => {
            class FooClass {
                constructor() {
                    this.foo = 'foo';
                }
            }
            class TestClass extends mixin(FooClass) {
                constructor() {
                    super();
                    this.foo = 'test';
                }
            }

            const instance = new TestClass();

            expect(instance).to.have.property('foo');
            expect(instance.foo).to.equal('test');
        });

        it('return a class with FooClass methods overridden', () => {
            class FooClass {
                foo() {
                    return 'foo';
                }
            }
            class TestClass extends mixin(FooClass) {
                foo() {
                    return 'test';
                }
            }

            const instance = new TestClass();

            expect(instance).to.have.property('foo');
            expect(instance.foo()).to.equal('test');
        });

        it('return a class with FooClass static properties', () => {
            class FooClass {
                static foo = 'foo';
            }
            class TestClass extends mixin(FooClass) {}

            expect(TestClass.foo).to.equal('foo');
        });

        it('return a class with FooClass static properties overriden', () => {
            class FooClass {
                static foo = 'foo';
            }
            class TestClass extends mixin(FooClass) {
                static foo = 'test';
            }

            expect(TestClass.foo).to.equal('test');
        });

        it('return a class with a specific constructor', () => {
            class FooClass {
                constructor() {
                    this.foo = 'foo';
                }
            }
            class TestClass extends mixin(FooClass) {
                constructor() {
                    super();
                    this.test = 'test';
                }
            }

            const instance = new TestClass();

            expect(instance.foo).to.equal('foo');
            expect(instance.test).to.equal('test');
        });
    });
});
